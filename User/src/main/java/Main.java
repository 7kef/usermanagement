import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.*;

public class Main {

    /**
     * user dictionary : inMemory user database, key - unique user login
     */
    private static final HashMap<String, User> userDict = new HashMap<>();
    /**
     * getting user input data
     */
    private static final Scanner sc = new Scanner(System.in);
    /**
     * current(logged) user
     */
    private static User currentUser = null;
    /**
     *  validator - validating inputed data
     */
    private static ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    private static Validator validator = factory.getValidator();
    /**
     * If user is logged?
     */
    private static boolean logged=false;

    //main function
    public static void main(String[] args) {
        //user for tests
        User v0va = new User();
        v0va.setLogin("7kef");
        v0va.setPassword("7kef!@#)#");
        v0va.setPasswordConfirm("7kef!@#)#");
        v0va.setEmail("7kef@example.com");
        v0va.setPhoneNumber("693003590");
        userDict.put("7kef", v0va);


        //validatin user
        Set<ConstraintViolation<User>> violations = validator.validate(v0va);
        for (ConstraintViolation<User> violation : violations) {
            System.err.println(violation.getMessage());
        }

        //Welcome message
        System.out.println(
                "Hello! Welcome to our service!If you want to use our service you must have an account");

        /**
         *  MENUS
         */

        String action;

        while (true) {
            /**
             * Main menu : loging in / registrating / exit program
             */
            while (!logged) {
                action = mainMenu();
                switch (action) {
                    case ("1"):
                        loginUser();
                        break;
                    case ("2"):
                        registerUser();
                        break;
                    case ("3"):
                        System.exit(100);
                        break;
                    default:
                        break;
                }
            }
            /**
             * User menu : change e-mail, phone-number /show account info/logout
             */
            while (logged) {
                action = loggedMenu();

                switch (action) {
                    case ("1"):
                        propertyInitialization(currentUser, "email");
                        break;
                    case ("2"):
                        propertyInitialization(currentUser, "phoneNumber");
                        break;
                    case ("3"):
                        System.out.println("\n"+currentUser.toString()+"\n");
                        break;
                    case ("4"):
                        currentUser = null;
                        logged = false;
                        break;
                    default:
                       break;
                }
            }
        }
    }

    /**Main menu
     *
     * @return user action
     */
    private static String mainMenu() {
        String action;
        System.out.println(
                "******MENU******"+
                "\nPlease type an option number [1-3]"+
                "\n1. Log in"+
                "\n2. Sign up"+
                "\n3. Exit"
        );
        System.out.print("Action : ");
        action = sc.nextLine();
        return action;
    }

    /**User menu
     *
     * @return user action
     */
    private static String loggedMenu(){
        String action;
        System.out.println("***Hello, "+ currentUser.getLogin() +" ***" +
                "\nPlease type an option number [1-3]"+
                "\n1. Change e-mail"+
                "\n2. Change phone number"+
                "\n3. Show my account information"+
                "\n4. Log out");
        System.out.print("Action : ");
        action = sc.nextLine();
        return action;
    }

    /**Registrating new user
     */
    public static void registerUser() {
        User user = new User();

        System.out.println("******REGISTRATION******");
        propertyInitialization(user, "login");
        System.out.println("We want you to feel secured. Generate strong password for you? :) Y/N(default)");
        String generatePass = sc.nextLine();
        if(generatePass.equalsIgnoreCase("y")){
            String password;
            user.setPassword(password=user.generatePassword());
            user.setPasswordConfirm(password);
            while (!verifyUserProperty(user,"password").isEmpty()){
                user.setPassword(password=user.generatePassword());
                user.setPasswordConfirm(password);
            }
            user.setPassword(user.generatePassword());
            System.out.println("This is your password : "+user.getPassword()+"\nPlease remember it. You will need it to log in");
        }else{
            propertyInitialization(user, "password");
            propertyInitialization(user, "passwordConfirm");
//            while(user.getPasswordConfirm()==null){
//                propertyInitialization(user, "passwordConfirm");
//            }
        }

        propertyInitialization(user, "email");
        propertyInitialization(user, "phoneNumber");

        System.out.println("******SUMMARY****** ");

        System.out.println(user.toString());
        userDict.put(user.getLogin(),user);

        currentUser = user;
        logged=true;
    }

    /**Login user
     */
    public static void loginUser(){

        System.out.println("******LOG IN******");
        System.out.print("Please enter your login : ");
        String login = sc.nextLine();

        if ((currentUser = userDict.get(login)) != null) {
            System.out.print("Please enter your password : ");
            String password = sc.nextLine();
            if (currentUser.getPassword().equals(password)) {
                System.out.println("Hello, " + currentUser.getLogin());
                logged=true;
            }else{
                System.out.println("Wrong password!");
            }
        }else{
            System.out.println("User is not registered! Please register first!");
        }
    }

    /**Verifying user properties
     *
     * @param user - User object
     * @param propertyPath - property name(String)
     * @return violationSet - set of validation errors or empty set if none
     */
    public static synchronized Set verifyUserProperty(User user, String propertyPath){
        Set<ConstraintViolation<User>> violationSet = validator.validateProperty(user, propertyPath);
        for (ConstraintViolation<User> violation : violationSet) {
            System.err.println(violation.getMessage());
        }
        return violationSet;
    }

    /**Initializing user properties(with validation)
     *
     * @param user - User object
     * @param property - property name(String)
     */
    private static synchronized void propertyInitialization(User user, String property) {

        System.out.print("Please enter your " + property + " : ");
        String propertyIn = sc.nextLine();
        switch (property){
            case ("login"):
                while(userDict.get(propertyIn)!=null){
                    System.out.print("\nThis login is already taken. Please try again : ");
                    propertyIn = sc.nextLine();
                } user.setLogin(propertyIn);break;
            case ("password"): user.setPassword(propertyIn); break;
            case ("passwordConfirm"): user.setPasswordConfirm(propertyIn); break;
            case ("email"): user.setEmail(propertyIn);break;
            case ("phoneNumber"): user.setPhoneNumber(propertyIn);break;
        }

        while (!verifyUserProperty(user,property).isEmpty()){

            System.out.print("Please enter your " + property + " : ");
            propertyIn = sc.nextLine();
            switch (property){
                case ("login"): user.setLogin(propertyIn);break;
                case ("password"): user.setPassword(propertyIn);break;
                case ("passwordConfirm"): break;
                case ("email"): user.setEmail(propertyIn);break;
                case ("phoneNumber"): user.setPhoneNumber(propertyIn);break;
            }
        }
    }

}


