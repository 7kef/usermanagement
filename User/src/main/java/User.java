import javax.validation.MessageInterpolator;
import javax.validation.constraints.*;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.ResourceBundle;

public class User {

    public User() {
        Locale.setDefault(Locale.US);

    }


    @NotNull(message = "{empty}")
    @Size(min=3, max = 12, message = "{login.size}")
    private String login;

    @NotNull(message = "{empty}")
    @Pattern(regexp = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\\Q!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~\\E]).{8,32}$", message = "{password.complex}")
    private String password;

    @NotNull(message = "{password.confirm.empty.or.wrong}")
    private String passwordConfirm;

    @NotNull(message = "{empty}")
    @Email(message = "{email.invalid}")
    private String email;



    @NotNull(message = "{empty}")
    @Digits(integer = 100, fraction = 0,  message = "{phone.digits}")
    @Size(min = 9, max = 11, message = "{phone.size}")
    private String phoneNumber;


    public String generatePassword(){
        char[] lowAlphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        char[] upperAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        char[] digits = "0123456789".toCharArray();
        char[] specialChar = "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~".toCharArray();
        Random generator = new Random();
        String password="";
        int listChoose;
        for(int length=0; length<10; length++){
            listChoose = generator.nextInt(4)+1;
            switch (listChoose){
                case (1): password+=lowAlphabet[generator.nextInt(lowAlphabet.length)];break;
                case (2): password+=upperAlphabet[generator.nextInt(upperAlphabet.length)];break;
                case (3): password+=digits[generator.nextInt(digits.length)];break;
                case (4): password+=specialChar[generator.nextInt(specialChar.length)];break;
                default:break;
            }
        }
        return password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(
            String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(
            String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(
            String passwordConfirm) {
        if(this.password.equals(passwordConfirm))
            this.passwordConfirm = passwordConfirm;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(
            String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(
            String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "USER: "+this.login+
                "\nE-MAIL: "+this.email+
                "\nPHONE NUMBER: "+this.phoneNumber;
    }
}
