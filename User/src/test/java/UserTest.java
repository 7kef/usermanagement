import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Set;

public class UserTest {


    private Locale locale = Locale.ENGLISH;

    private ResourceBundle validationMessages = ResourceBundle.getBundle("ValidationMessages", locale);


    private static ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    private static Validator validator = factory.getValidator();

    private Set<ConstraintViolation<User>> resultSet = null;
    private Main mainProgram = new Main();



    //VALIDATION TESTING

    //Login tests
    @Test
    public void loginMustBeAtLeast3CharLong(){
        User user = new User();
        user.setLogin("aa");
        resultSet = (Set<ConstraintViolation<User>> ) mainProgram.verifyUserProperty(user,"login");
        Assert.assertFalse(resultSet.isEmpty());
        for (ConstraintViolation<User> violation : resultSet) {
            Assert.assertEquals(validationMessages.getString("login.size"),violation.getMessage());
        }

    }

    @Test
    public void loginCanNotBeNull(){
        User user = new User();
        user.setLogin(null);
        resultSet = (Set<ConstraintViolation<User>> ) mainProgram.verifyUserProperty(user,"login");
        Assert.assertFalse(resultSet.isEmpty());
        for (ConstraintViolation<User> violation : resultSet) {
            Assert.assertEquals(validationMessages.getString("empty"),violation.getMessage());
        }
    }
    @Test
    public void loginMaxLengthIs12(){
        User user = new User();
        user.setLogin("aaaaaaaaaaaaa");
        resultSet = (Set<ConstraintViolation<User>> ) mainProgram.verifyUserProperty(user,"login");
        Assert.assertFalse(resultSet.isEmpty());
        for (ConstraintViolation<User> violation : resultSet) {
            Assert.assertEquals(validationMessages.getString("login.size"),violation.getMessage());
        }
    }




    //PASSWORD tests
    @Test
    public void passwordMustHaveUpperCaseLetter(){
        User user = new User();
        user.setPassword("aaa123!@#");
        resultSet = (Set<ConstraintViolation<User>> ) mainProgram.verifyUserProperty(user,"password");
        Assert.assertFalse(resultSet.isEmpty());
        for (ConstraintViolation<User> violation : resultSet) {
            Assert.assertEquals(validationMessages.getString("password.complex"),violation.getMessage());
        }
    }
    @Test
    public void passwordMustHaveDigit(){
        User user = new User();
        user.setPassword("AAAaaa!@#");
        resultSet = (Set<ConstraintViolation<User>> ) mainProgram.verifyUserProperty(user,"password");
        Assert.assertFalse(resultSet.isEmpty());
        for (ConstraintViolation<User> violation : resultSet) {
            Assert.assertEquals(validationMessages.getString("password.complex"),violation.getMessage());
        }
    }
    @Test
    public void passwordMustHaveSpecialChar(){
        User user = new User();
        user.setPassword("AAAaaa123");
        resultSet = (Set<ConstraintViolation<User>> ) mainProgram.verifyUserProperty(user,"password");
        Assert.assertFalse(resultSet.isEmpty());
        for (ConstraintViolation<User> violation : resultSet) {
            Assert.assertEquals(validationMessages.getString("password.complex"),violation.getMessage());
        }
    }
    @Test
    public void passwordCanNotBeNull(){
        User user = new User();
        user.setPassword(null);
        resultSet = (Set<ConstraintViolation<User>> ) mainProgram.verifyUserProperty(user,"password");
        Assert.assertFalse(resultSet.isEmpty());
        for (ConstraintViolation<User> violation : resultSet) {
            Assert.assertEquals(validationMessages.getString("empty"),violation.getMessage());
        }
    }
    @Test
    public void passwordMustBeMin8CharLong(){
        User user = new User();
        user.setPassword("Aa12!@");
        resultSet = (Set<ConstraintViolation<User>> ) mainProgram.verifyUserProperty(user,"password");
        Assert.assertFalse(resultSet.isEmpty());
        for (ConstraintViolation<User> violation : resultSet) {
            Assert.assertEquals(validationMessages.getString("password.complex"),violation.getMessage());
        }
    }
    @Test
    public void passwordMustBeMax32CharLong(){
        User user = new User();
        user.setPassword("Aaaaaabbbbbbccc1234567890!@#$%^&*()qqqqqqqqqqq2qq32432");
        resultSet = (Set<ConstraintViolation<User>> ) mainProgram.verifyUserProperty(user,"password");
        Assert.assertFalse(resultSet.isEmpty());
        for (ConstraintViolation<User> violation : resultSet) {
            Assert.assertEquals(validationMessages.getString("password.complex"),violation.getMessage());
        }
    }

    @Test
    public void passwordConfirmMustMatchPassword(){
        User user = new User();
        user.setPassword("AAAaaa123!@#");
        user.setPassword("Aaa123!@#");
        resultSet = (Set<ConstraintViolation<User>> ) mainProgram.verifyUserProperty(user,"passwordConfirm");
        Assert.assertFalse(resultSet.isEmpty());
        for (ConstraintViolation<User> violation : resultSet) {
            Assert.assertEquals(validationMessages.getString("password.confirm.empty.or.wrong"),violation.getMessage());
        }
    }

    //EMAIL field test
    @Test
    public void emailMustcontainAtSignSymbol(){
        User user = new User();
        user.setEmail("vvv.gmail.com");
        resultSet = (Set<ConstraintViolation<User>> ) mainProgram.verifyUserProperty(user,"email");
        Assert.assertFalse(resultSet.isEmpty());
        for (ConstraintViolation<User> violation : resultSet) {
            Assert.assertEquals(validationMessages.getString("email.invalid"),violation.getMessage());
        }
    }

    //PHONE NUMBER field test
    @Test
    public void phoneMustContainOnlyDigits(){
        User user = new User();
        user.setPhoneNumber("bla bla bla");
        resultSet = (Set<ConstraintViolation<User>> ) mainProgram.verifyUserProperty(user,"phoneNumber");
        Assert.assertFalse(resultSet.isEmpty());
        for (ConstraintViolation<User> violation : resultSet) {
            Assert.assertEquals(validationMessages.getString("phone.digits"),violation.getMessage());
        }
    }
    @Test
    public void phoneMustBeMin9DigitLong(){
        User user = new User();
        user.setPhoneNumber("112");
        resultSet = (Set<ConstraintViolation<User>> ) mainProgram.verifyUserProperty(user,"phoneNumber");
        Assert.assertFalse(resultSet.isEmpty());
        for (ConstraintViolation<User> violation : resultSet) {
            Assert.assertEquals(validationMessages.getString("phone.size"),violation.getMessage());
        }
    }
    @Test
    public void phoneMustBeMax11DigitLong(){
        User user = new User();
        user.setPhoneNumber("354256798123457");
        resultSet = (Set<ConstraintViolation<User>> ) mainProgram.verifyUserProperty(user,"phoneNumber");
        Assert.assertFalse(resultSet.isEmpty());
        for (ConstraintViolation<User> violation : resultSet) {
            Assert.assertEquals(validationMessages.getString("phone.size"),violation.getMessage());
        }
    }
}
